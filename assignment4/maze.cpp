//Eric Schultz
//02/04/15
//CIS 330 Assignment 4

#include "maze.hpp"

Maze::Maze(int size) {

	num = size;
	maze = new char* [num];
	for(int i = 0; i < num; i++) {
		maze[i] = new char [num];
	}

}

void Maze::readFromFile(std::ifstream &f) {

	int i = 0;

	//read in maze itself
	for(i = 0; i < num; i++) {
		for(int j = 0; j < num; j++) {
			f >> maze[i][j];
		}
	}

	//find entrance and exit
	for(i = 0; i < num; i++) {
		if(maze[i][0] == 'x') {
			currentRow = i;
			currentCol = 0;
			facing = RIGHT;
		}
		if(maze[i][0] == '.') {
			endRow = i;
			endCol = 0;
		}
		if(maze[i][num - 1] == 'x') {
			currentRow = i;
			currentCol = num - 1;
			facing = LEFT;
		}
		if(maze[i][num - 1] == '.') {
			endRow = i;
			endCol = num - 1;
		}
		if(maze[0][i] == 'x') {
			currentRow = 0;
			currentCol = i;
			facing = DOWN;
		}
		if(maze[0][i] == '.') {
			endRow = 0;
			endCol = i;
		}
		if(maze[num - 1][i] == 'x') {
			currentRow = num - 1;
			currentCol = i;
			facing = UP;
		}
		if(maze[num - 1][i] == '.') {
			endRow = num - 1;
			endCol = i;
		}
	}

}

void Maze::getCurrentPosition(int &row, int &col) {

	//simply set row and col equal to the current position.
	row = currentRow;
	col = currentCol;
	
}

void Maze::step() {

	if(facing == UP) {
		if(maze[currentRow][currentCol + 1] == '.') {
			currentCol += 1;
			facing = RIGHT;
			cout << "RIGHT" << endl;
			return;
		}
		if(maze[currentRow - 1][currentCol] == '.') {
			currentRow -= 1;
			cout << "UP" << endl;
			return;
		}
		if(maze[currentRow][currentCol - 1] == '.') {
			currentCol -= 1;
			facing = LEFT;
			cout << "LEFT" << endl;
			return;
		} else {
			currentRow += 1;
			facing = DOWN;
			cout << "DOWN" << endl;
			return;
		}
	}

	if(facing == DOWN) {
		if(maze[currentRow][currentCol - 1] == '.') {
			currentCol -= 1;
			facing = LEFT;
			cout << "LEFT" << endl;
			return;
		}
		if(maze[currentRow + 1][currentCol] == '.') {
			currentRow += 1;
			cout << "DOWN" << endl;
			return;
		}
		if(maze[currentRow][currentCol + 1] == '.') {
			currentCol += 1;
			facing = RIGHT;
			cout << "RIGHT" << endl;
			return;
		} else {
			currentRow -= 1;
			facing = UP;
			cout << "UP" << endl;
			return;
		}
	}

	if(facing == LEFT) {
		if(maze[currentRow - 1][currentCol] == '.') {
			currentRow -= 1;
			facing = UP;
			cout << "UP" << endl;
			return;
		}
		if(maze[currentRow][currentCol - 1] == '.') {
			currentCol -= 1;
			cout << "LEFT" << endl;
			return;
		}
		if(maze[currentRow + 1][currentCol] == '.') {
			currentRow += 1;
			facing = DOWN;
			cout << "DOWN" << endl;
			return;
		} else {
			currentCol += 1;
			facing = RIGHT;
			cout << "RIGHT" << endl;
			return;
		}
	}

	if(facing == RIGHT) {
		if(maze[currentRow + 1][currentCol] == '.') {
			currentRow += 1;
			facing = DOWN;
			cout << "DOWN" << endl;
			return;
		}
		if(maze[currentRow][currentCol + 1] == '.') {
			currentCol += 1;
			cout << "RIGHT" << endl;
			return;
		}
		if(maze[currentRow - 1][currentCol] == '.') {
			currentRow -= 1;
			facing = UP;
			cout << "UP" << endl;
			return;
		} else {
			currentCol -= 1;
			facing = LEFT;
			cout << "LEFT" << endl;
			return;
		}
	}

}

bool Maze::atExit() {

	if(currentRow == endRow && currentCol == endCol) {
		return true;
	}
	return false;

}
