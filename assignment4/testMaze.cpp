//Eric Schultz
//02/04/15
//CIS 330 Assignment4 

#include <iostream>
#include <fstream>
#include "maze.hpp"

int main(int argc, const char* argv[]) {
	if( argc != 2 ) //checks for the input file name
	{
		std::cerr << "Error: no input file name" << std::endl;
		std::cerr << "Usage: ./" << argv[0] << " someinput.txt" << std::endl;
		return 1;
	}

	std::ifstream mazeInputFile ( argv[1] );
	int numMazes = 0;
	mazeInputFile >> numMazes;

	for (int i = 0; i < numMazes; i++ ) {
		int mazeSize = 0;
		mazeInputFile >> mazeSize;
		if (mazeSize < 10 || mazeSize > 30) {
			std::cerr << "Error: invalid maze size " << mazeSize << " read from " << argv[1] << std::endl;
			std::cerr << "     Maze sizes must be between 10 and 30" << std::endl;
			return 1;
		}

		Maze maze(mazeSize);

		maze.readFromFile(mazeInputFile);
		
		std::cout << "START" << std::endl;

		do {
			// Advance one step in the maze
			maze.step();

		} while ( ! maze.atExit() );

		std::cout << "EXIT" << std::endl << "***" << std::endl;

	}

	return 0;
}
