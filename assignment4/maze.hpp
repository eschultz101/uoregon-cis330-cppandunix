/*
 * maze.hpp
 *      Author: norris
 */

#ifndef MAZE_HPP_
#define MAZE_HPP_

#include <iostream>
#include <fstream>
using namespace std;

class Maze
{
public:
	Maze(int size);
	~Maze() {}

	typedef enum Direction { DOWN, RIGHT, UP, LEFT } DIRECTION;

	// Implement the following functions:

	// read maze from file, find starting location
	void readFromFile(ifstream &f);

	// make a single step advancing toward the exit
	void step();

	// return true if the maze exit has been reached, false otherwise
	bool atExit();

	// set row and col to current position of 'x'
	void getCurrentPosition(int &row, int &col);

	// You can add more functions if you like
private:
	int num = 0, endRow = 0, endCol = 0, currentRow = 0, currentCol = 0;
	char** maze = NULL;
	DIRECTION facing = UP;
	
};


#endif /* MAZE_HPP_ */
