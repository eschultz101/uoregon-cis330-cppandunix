//Eric Schultz
//1/28/15
//CIS330 Assignment3

#include <stdio.h>
#include <stdlib.h>

void createMazes(char****, int*, char*, int**);
void traverseMaze(char**, int);
int recursiveSolve(char**, int, int, int, int, int, int***, int***);

int main(int argc, char* argv[]) {

	char*** mazes = NULL;
	int* sizes = NULL;
	int numMazes = 0;

	createMazes(&mazes, &numMazes, argv[1], &sizes);
	
	for(int i = 0; i < numMazes; i++) {
		traverseMaze(mazes[i], sizes[i]);
	}
	
	return 0;
}

void createMazes(char**** mazes, int* numMazes, char* inputFile, int** sizes) {

	FILE* fpIn = fopen(inputFile, "r");

	fscanf(fpIn, "%d\n", &(*numMazes));
	(*mazes) = (char***)malloc((*numMazes) * sizeof(char**));
	(*sizes) = (int*)malloc((*numMazes) * sizeof(char));
	for(int i = 0; i < (*numMazes); i++) {
		fscanf(fpIn, "%d\n", &((*sizes)[i]));
		(*mazes)[i] = (char**)malloc(((*sizes)[i]) * sizeof(char*));
		for(int j = 0; j < (*sizes)[i]; j++) {
			(*mazes)[i][j] = (char*)malloc(((*sizes)[i]) * sizeof(char));
			for(int k = 0; k < (*sizes)[i]; k++) {
				fscanf(fpIn," %c", &((*mazes)[i][j][k]));
			}
		}
	}

	fclose(fpIn);

}

void traverseMaze(char** maze, int size) {

	int position[2], _exit[2];
	int** wasHere = (int**)malloc(size * sizeof(int*));
	int** correctPath = (int**)malloc(size * sizeof(int*));
	for(int k = 0; k < size; k++){
		wasHere[k] = (int*)malloc(size * sizeof(int));
		correctPath[k] = (int*)malloc(size * sizeof(int));
		for(int m = 0; m < size; m++) {
			wasHere[k][m] = 0;
			correctPath[k][m] = 0;
		}
	}

	//find entrance and exit to maze
	for(int i = 0; i < size; i++) {
		if(maze[i][0] == 'x') {
			position[0] = i;
			position[1] = 0;
		}
		if(maze[i][size - 1] == 'x') {
			position[0] = i;
			position[1] = size - 1;
		}
		if(maze[i][0] == '.') {
			_exit[0] = i;
			_exit[1] = 0;
		}
		if(maze[i][size - 1] == '.') {
			_exit[0] = i;
			_exit[1] = size - 1;
		}
	}
	for(int j = 0; j < size; j++) {
		if(maze[0][j] == 'x') {
			position[0] = 0;
			position[1] = j;
		}
		if(maze[size - 1][j] == 'x') {
			position[0] = size - 1;
			position[1] = j;
		}
		if(maze[0][j] == '.') {
			_exit[0] = 0;
			_exit[1] = j;
		}
		if(maze[size - 1][j] == '.') {
			_exit[0] = size - 1;
			_exit[1] = j;
		}
	}

	printf("ENTER\n");
	recursiveSolve(maze, size, _exit[0], _exit[1], position[0], position[1], &wasHere, &correctPath);
	printf("EXIT\n***\n");

	return;
}

int recursiveSolve(char** maze, int size, int x, int y, int endX, int endY, int*** wasHere, int*** correctPath) {

	if(x == endX && y == endY) return 1;
	if(maze[x][y] == '@' || (*wasHere)[x][y] == 1) {return 0;}
	(*wasHere)[x][y] = 1;
	if(x != 0) {
		if(recursiveSolve(maze, size, x - 1, y, endX, endY, wasHere, correctPath) == 1) {
			(*correctPath)[x][y] = 1;
			printf("DOWN\n");
			return 1;
		}
	}
	if(x != size - 1) {
		if(recursiveSolve(maze, size, x + 1, y, endX, endY, wasHere, correctPath) == 1) {
			(*correctPath)[x][y] = 1;
			printf("UP\n");
			return 1;
		}
	}
	if(y != 0) {
		if(recursiveSolve(maze, size, x, y - 1, endX, endY, wasHere, correctPath) == 1) {
			(*correctPath)[x][y] = 1;
			printf("RIGHT\n");
			return 1;
		}
	}
	if(y != size - 1) {
		if(recursiveSolve(maze, size, x, y + 1, endX, endY, wasHere, correctPath) == 1) {
			(*correctPath)[x][y] = 1;
			printf("LEFT\n");
			return 1;
		}
	}
	return 0;

}
