// Eric Schultz
// 2/25/2015
// CIS 330 Assignment 6

#include <fstream>
#include <string>
#include <map>

#include "mr.hpp"

namespace mr {

class SentenceStats : public MapReduce<std::string,int> {
public:
	virtual void MRmap(const std::map<std::string,std::string> &input,
			std::multimap<std::string,int> &out_values);
	virtual void MRreduce(const std::multimap<std::string,int> &intermediate_values,
			std::map<std::string,int> &out_values);
private:
	int max = 1, min = 1000;
	float avg = 0;
}; 

} // namespace mr

