// Eric Schultz
// 2/25/2015
// CIS 330 Assignment 6

#include <iostream>
#include <string>
#include <algorithm>
#include <map>

#include "mr.hpp"
#include "sentenceStats.hpp"
#include "ioutils.hpp"

namespace mr {

void
SentenceStats::MRmap(const std::map<std::string,std::string> &input,
				std::multimap<std::string,int> &out_values) {

	IOUtils io;

	for (auto it = input.begin(); it != input.end(); it++ ) {
		std::string inputString = io.readFromFile(it->first);

		std::istringstream iss(inputString);
		std::string word = "";
		std::string sentence = "";
		std::vector<std::string> v_words;
		int numWords = 0;

		while (iss >> word) {
			v_words.push_back(word);
			//std::cout << "WORD: " << word << std::endl;
			if(word.back() == '.' || word.back() == '?' || word.back() == '!') {
				for(auto it = v_words.begin(); it != v_words.end(); ++it) {
					sentence.append(*it);
					sentence.append(" ");
				}
				numWords = v_words.size();
				out_values.insert(std::pair<std::string,int>(sentence, numWords));
				v_words.clear();
				sentence = "";
			}
		}
	}

}

void
SentenceStats::MRreduce(const std::multimap<std::string,int> &intermediate_values,
					std::map<std::string,int> &out_values) {

	int sum = 0, count = 0;

	std::for_each(intermediate_values.begin(), intermediate_values.end(),
			//enter min finding function here
			[&](std::pair<std::string,int> mapElement)->void
			{
				if(mapElement.second < min) {min = mapElement.second;}
			});  // end of for_each
	std::for_each(intermediate_values.begin(), intermediate_values.end(),
			//enter max finding function here
			[&](std::pair<std::string,int> mapElement)->void
			{
				if(mapElement.second > max) {max = mapElement.second;}
			});  // end of for_each
	std::for_each(intermediate_values.begin(), intermediate_values.end(),
			//enter avg finding function here
			[&](std::pair<std::string,int> mapElement)->void
			{
				++count;
				sum += mapElement.second;
				avg = sum/count;
			});  // end of for_each

	std::cout << "Minimum sentence length: " << min << std::endl; 
	std::cout << "Maximum sentence length: " << max << std::endl;
	std::cout << "Average sentence length: " << avg << std::endl;

}

} // namespace mr
