// Eric Schultz
// 2/25/2015
// CIS 330 Assignment 6

#include <iostream>
#include <string>
#include <algorithm>
#include <map>

#include "mr.hpp"
#include "phrases.hpp"
#include "ioutils.hpp"

namespace mr {

void
Phrases::MRmap(const std::map<std::string,std::string> &input,
				std::multimap<std::string,int> &out_values) {

	IOUtils io;
	char* chars = new char [10];
	chars = ".,\"-?!()@;\0";

	for (auto it = input.begin(); it != input.end(); it++ ) {
		std::string inputString = io.readFromFile(it->first);
		//clean inputString
		for(int i = 0; i < 9; i++ ) {
			inputString.erase(std::remove(inputString.begin(), inputString.end(), chars[i]), inputString.end());
		}

		// find all two word phrases
		std::istringstream iss(inputString);
		do {
			std::string word, word2;
			std::string phrase = "";
			iss >> word;
			iss >> word2;
			phrase.append(word);
			phrase.append(" ");
			phrase.append(word2);

			// Each word gets assigned a count of 1
			out_values.insert(std::pair<std::string,int>(phrase, 1));
		} while (iss);
	}

}

void
Phrases::MRreduce(const std::multimap<std::string,int> &intermediate_values,
					std::map<std::string,int> &out_values) {

	std::for_each(intermediate_values.begin(), intermediate_values.end(),
			[&](std::pair<std::string,int> mapElement)->void
			{
				out_values[mapElement.first] += 1;
			});

}

}

