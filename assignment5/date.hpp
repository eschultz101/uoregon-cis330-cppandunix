#ifndef DATE_HPP_
#define DATE_HPP_

#include "cipher.hpp"

class DateCipher : public Cipher {
public:
	DateCipher();
	virtual ~DateCipher();
	virtual std::string encrypt( std::string &text );
	virtual std::string decrypt( std::string &text );
private:
	std::string date;
	std::string alphabet;
	std::string dateStr;
};

#endif
