// Eric Schultz
// CIS 330 Assignment 5

#include "cipher.hpp"
#include "caesar.hpp"

CaesarCipher::CaesarCipher() : Cipher(), key(7) {
}

CaesarCipher::~CaesarCipher() {
}

std::string
CaesarCipher::encrypt(std::string &inputText) {
	std::string text = inputText;
	std::string::size_type len = text.length();
	for(int i = 0; i != len; i++) {
		text[i] = text[i] - key; //include spaces?
	}
	return text;

}

std::string
CaesarCipher::decrypt(std::string &text) {
	std::string::size_type len = text.length();
	for(int i = 0; i != len; i++) {
		text[i] = text[i] + key; //include spaces?
	}
	return text;
}

