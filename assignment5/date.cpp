// Eric Schultz
// CIS 330 Assignment 5

#include "cipher.hpp"
#include "date.hpp"
#include <iostream>
#include <algorithm>

DateCipher::DateCipher() : Cipher(), date("91493") {
	alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ,.':()!+;";
	alphabet[62] = '\n';
	alphabet[63] = '\0';
}

DateCipher::~DateCipher() {
}

std::string
DateCipher::encrypt(std::string &inputText) {
	std::string text = inputText;
	std::string::size_type dateLen = date.length();
	std::string::size_type len = text.length();
	int* textAsNumbers = new int [len];
	dateStr = text;
	for(int i = 0; i < len; i++) {
		for(int j = 0; j < 63; j++) {
			if(text[i] == alphabet[j]) {
				textAsNumbers[i] = j;
			}
		}
		dateStr[i] = date[i % dateLen];
	}
	
	for(int i = 0; i < len; i++) {
		text[i] = alphabet[ (textAsNumbers[i] + (dateStr[i] - '0')) % 63 ];
	}

	return text;

}

std::string
DateCipher::decrypt(std::string &text) {
	std::string::size_type len = text.length();
	int* textAsNumbers = new int [len];
	for(int i = 0; i < len; i++) {
		for(int j = 0; j < 63; j++) {
			if(text[i] == alphabet[j]) {
				textAsNumbers[i] = j;
			}
		}
	}

	for(int i = 0; i < len; i++) {
		text[i] = alphabet[ (((textAsNumbers[i] - (dateStr[i] - '0')) % 63) + 63) % 63 ];
	}

	return text;

}
