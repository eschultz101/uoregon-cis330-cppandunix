
#include <stdio.h>
#include <stdlib.h>

void allocateNumberSquare(const int size, char*** square) {

	int i;
	*square = (char**)malloc(size * sizeof(char*));
	for(i = 0; i < size; i++) {
		(*square)[i] = (char*)malloc(size * sizeof(char));
	}

}

void initializeNumberSquare(const int size, char** square) {

	int i, j;
	for(i = 0; i < size; i++) {
		for(j = 0; j < size; j++) {
			square[i][j] = '0' + j;
		}
	}
	
}

void printNumberSquare(const int size, char** square) {

	int i, j;

	for(i = 0; i < size; i++) {
		for(j = 0; j < size; j++) {
			printf("%c ", square[i][j]);
			if((j + 1) % size == 0)
				printf("\n");
		}
	}

}

void deallocateNumberSquare(const int size, char** square) {

	int i;
	
	for(i = 0; i < size; i++) {
		free(square[i]);
	}

	free(square);

}

