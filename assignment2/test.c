#include "square2.h"
#include "diamond.h"
#include <stdio.h>

int main(void) {

	char** square;
	int size;

	printf("What size will the square be (2-10)? ");
	scanf("%d%*c", &size);

	allocateNumberSquare((const int)size, &square);
	initializeNumberSquare((const int)size, square);
	printNumberDiamond((const int)size, square);
	deallocateNumberSquare((const int)size, square);

	return 0;

}

