
#include <stdio.h>

void printNumberDiamond(const int size, char **square) {

	int i, j, k;

	for(i = 0; i < size; i++) {

		for(k = 0; k < (size - i); k++) {
			printf(" ");
		}

		for(j = 0; j < size - (size - i); j++) {
			printf("%c ", square[i][j]);
		}

		printf("\n");
	}

	for(i = 0; i < size; i++) {
	
		for(k = 0; k < (size - (size - i)); k++) {
			printf(" ");
		}

		for(j = 0; j < (size - i); j++) {
			printf("%c ", square[i][j]);
		}

		printf("\n");
	}

}


