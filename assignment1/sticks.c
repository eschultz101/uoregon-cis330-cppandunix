#include <stdio.h>
#include <stdlib.h>
#include <time.h>      
#include <string.h>

int getHumanChoice() {
     
	int num = 0;
	
	while(num > 3 || num < 1) {
		printf("Player 1: How many sticks do you take (1-3)?");
		scanf("%d", &num);
		if(num > 3 || num < 1)
			printf("\nError: incorrect number of sticks entered\n");
	}

	return num;	
}

int getComputerChoice(int current_sticks) {
    
    /* get a pseudo-random integer between 1 and 3 (inclusive) */
    int rand_choice = rand() % 3 + 1;
    
    if (rand_choice > current_sticks) return current_sticks;

    /* Optionally replace with additional logic for the computer's strategy */
    
    return rand_choice;
}


int main(int argc, char** argv) 
{
    int human, computer, number_sticks;
    
    
    srand (time(NULL)); /* for reproducible results, you can call srand(1); */
    
    printf("Welcome to the game of sticks!\n");
    printf("How many sticks are there on the table initially (10-100)? ");
    scanf("%d", &number_sticks);

    /* TODO: check that number_sticks is between 10 and 100 (inclusive) and either 
     print an error and exit if invalid value is entered or use a loop to keep 
     inputing number_sticks until a valid value is entered.
     */
     
	if(number_sticks < 10 || number_sticks > 100) {
		printf("\nIncorrect number of sticks entered!\n");
		exit(100);
	}

    /* TODO: Main game loop:
      While some sticks remain:
        1. Print the current number of sticks, e.g., "There are 2 sticks on the board."
        2. Human: Get number of sticks by calling getHumanChoice and update number_sticks
        3. Computer: get number of sticks by calling getComputerChoice and update number_sticks
        4. Output the computer's choice, e.g., "Computer selects 3."
      After all sticks are gone, output the result of the game, e.g., "You win!" or "You lose!"
     */

	while(number_sticks > 0) {
		printf("There are %d sticks on the board.\n", number_sticks);
		human = getHumanChoice();	
		number_sticks = number_sticks - human;
		
		if(number_sticks == 0) {
			printf("You win!\n");
			exit(0);
		}

		computer = getComputerChoice(number_sticks);
		number_sticks = number_sticks - computer;
		
		printf("Computer selects %d sticks.\n", computer);
		
		if(number_sticks == 0) {
			printf("You lose!\n");
			exit(0);
		}	
		
	}
    
    return 0;
}

