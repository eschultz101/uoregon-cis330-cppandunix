
#include <stdio.h>

void func(int *ptr);

#define z 3

void func(int *ptr){
	*ptr = 3;
}

int main() {
	
	int z = 1;
	func(&z);
	printf("%d\n", z);
	
	return 0;
}