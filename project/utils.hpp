// Eric Schultz
// 3/8/15
// CIS330 Final Project
// Input checking utilities

class Utils {
public:
	Utils();
	char** initBoard();
	bool checkPlacement(int x1, int x2, int y1, int y2, int req, char** board);
	void printBoard(char** board);
	bool gameOver(int playerHits, int enemyHits);
};
