// Eric Schultz
// 2/16/15
// CIS330 final project
// Battleship player class

using namespace std;

#include "player.hpp"
#include "utils.hpp"
#include <iostream>

Player::Player() {}

void Player::placeShips() {

	// give player power to pick where each ship goes; aircraft carrier(5), battleship(4), 
	// destroyer(3), pt boat(2), choose tip and end locations then fill in the rest, 
	// don't allow for overlapping ships...
	int rh = 0, ch = 0, rt = 0, ct = 0;
	bool valid = false;
	Utils utils;

	cout << "Ships must be placed inside the boundaries of the ocean, a 10x10 grid.\n";
	cout << "Use numbers 1-10 to specify coordinates (row enter col enter).\n\n";

	for(int i = 4; i > 0; --i){

		while(valid == false) {

			valid = true;

			if(i == 4) cout << "Place your Aircraft Carrier (5 units long):\n";
			if(i == 3) cout << "Place your Battleship (4 units long):\n";
			if(i == 2) cout << "Place your Destroyer (3 units long):\n";
			if(i == 1) cout << "Place your PT Boat (2 units long):\n";

			cout << "Front: " << endl;
			cin >> rh; --rh;
			cin >> ch; --ch;
			cout << "Back: " << endl;
			cin >> rt; --rt;
			cin >> ct; --ct;

			valid = utils.checkPlacement(rh, rt, ch, ct, i, board);

		}

		// actually change the board
		if(rh == rt) {
			if(ch > ct) {
				for(int i = ct; i <= ch; ++i) {
					board[rh][i] = '@';
				}
			} else {
				for(int i = ch; i <= ct; ++i) {
					board[rh][i] = '@';
				}
			}
		}

		if(ch == ct) {
			if(rh > rt) {
				for(int i = rt; i <= rh; ++i) {
					board[i][ch] = '@';
				}
			} else {
				for(int i = rh; i <= rt; ++i) {
					board[i][ch] = '@';
				}
			}
		}

		// print player's board
		utils.printBoard(board);

		valid = false;

	} // end for

	
} // end placeShips()

void Player::yourTurn(char** enemyBoard, char** enemyShipBoard) {

	// give player power to fire where he/she pleases on the enemy board

	int r = 0, c = 0;
	bool valid = false;
	Utils utils;

	while(valid == false) {

		valid = true;

		cout << "Choose a spot to fire at in enemy seas (row enter col enter):\n";
		cin >> r;
		cin >> c;

		if(r > 10 || r < 1 || c > 10 || c < 1)
			valid = false;

		if(enemyBoard[r - 1][c - 1] == '+' || enemyBoard[r - 1][c - 1] == 'O')
			valid = false;

	}

	if(enemyShipBoard[r - 1][c - 1] == '@') {
		enemyBoard[r - 1][c - 1] = '+';
		utils.printBoard(enemyBoard);
		cout << "You've hit an enemy ship!\n\n";
		++enemyHits;
	}

	if(enemyShipBoard[r - 1][c - 1] == '~') {
		enemyBoard[r - 1][c - 1] = 'O';
		utils.printBoard(enemyBoard);
		cout << "You missed...\n\n";
	}

}
