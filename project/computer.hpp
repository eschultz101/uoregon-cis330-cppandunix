// Eric Schultz
// 2/16/15
// CIS330 final project
// Battleship computer header

class Computer {
public:
	Computer();
	void compPlaceShips();
	void compTurn(char**);

	char** compBoard;	// that player can see, hits and misses only
	char** compShipBoard;	// invisible to player, only used to check ship locations
	int lastMove[3] = {-1, -1, 0};	// holds coords of last move and whether or not it was a hit
	int enemyHits = 0;	// number of times enemy ships have been hit
};