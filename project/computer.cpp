// Eric Schultz
// 2/16/15
// CIS330 final project
// Battleship computer methods

using namespace std;

#include "computer.hpp"
#include "player.hpp"
#include "utils.hpp"
#include <iostream>
#include <ctime>
#include <cstdlib>

Computer::Computer() {
	srand( time(NULL) );
}

void Computer::compPlaceShips() {

	// place computer's ships pseudo-randomly on compShipBoard, without overlapping
	Utils utils;
	bool valid = false;
	int xh = 0, yh = 0, xt = 0, yt = 0;
	unsigned rNum1 = 0, rNum2 = 0;

	for(int i = 4; i > 0; --i) {

		while(valid == false) {

			valid = true;

			rNum1 = rand() % 10;
			rNum2 = rand() % 10;

			if(rNum1 > 5) {		// ship placed vertically
				xh = rNum1; 
				yh = rNum2; 
				xt = xh - i; 
				yt = yh; 
			} else {			// ship placed horizontally
				yh = rNum1; 
				xh = rNum2; 
				yt = yh + i; 
				xt = xh; 
			}

			valid = utils.checkPlacement(xh, xt, yh, yt, i, compShipBoard);

		}

		if(xh == xt) {
			if(yh > yt) {
				for(int i = yt; i <= yh; ++i) {
					compShipBoard[xh][i] = '@';
				}
			} else {
				for(int i = yh; i <= yt; ++i) {
					compShipBoard[xh][i] = '@';
				}
			}
		}

		if(yh == yt) {
			if(xh > xt) {
				for(int i = xt; i <= xh; ++i) {
					compShipBoard[i][yh] = '@';
				}
			} else {
				for(int i = xh; i <= xt; ++i) {
					compShipBoard[i][yh] = '@';
				}
			}
		}

		valid = false;

	}
}

void Computer::compTurn(char** enemyBoard) {

	// process computer's firing turn using knowledge of players board, hits and misses that is
	// and set up nextMove
	// if player gets hit, increment enemyHits

	int r = 0, c = 0;
	bool valid = false;
	Utils utils;

	if(lastMove[2] == 0) {	// if the last move the computer made missed...

		while(valid == false) {
			
			valid = true;

			// fire randomly
			r = rand() % 10;
			c = rand() % 10;

			if(enemyBoard[r][c] == 'O' || enemyBoard[r][c] == '+')
				valid = false;

		}

	} else if(lastMove[2] == 1) {	// if the last move the computer made hit...

		// check above the last move
		if(lastMove[0] - 1 >= 0 && enemyBoard[lastMove[0] - 1][lastMove[1]] != '+' &&
				enemyBoard[lastMove[0] - 1][lastMove[1]] != 'O') {
			r = lastMove[0] - 1;
			c = lastMove[1];		// OFF BY ONE?

		// check to the right of the last move
		} else if(lastMove[1] + 1 < 10 && enemyBoard[lastMove[0]][lastMove[1] + 1] != '+' &&
				enemyBoard[lastMove[0]][lastMove[1] + 1] != 'O') {
			r = lastMove[0];
			c = lastMove[1] + 1;

		// check below the last move
		} else if(lastMove[0] + 1 < 10 && enemyBoard[lastMove[0] + 1][lastMove[1]] != '+' &&
				enemyBoard[lastMove[0] + 1][lastMove[1]] != 'O') {
			r = lastMove[0] + 1;
			c = lastMove[1];

		// check to the left of the last move
		} else if(lastMove[1] - 1 >= 0 && enemyBoard[lastMove[0]][lastMove[1] - 1] != '+' &&
				enemyBoard[lastMove[0]][lastMove[1] - 1] != 'O') {
			r = lastMove[0];
			c = lastMove[1] - 1;

		// if all positions are invalid for whatever reason, just fire random shot
		} else {

			valid = false;

			while(valid == false) {
			
				valid = true;

				r = rand() % 10;
				c = rand() % 10;

				if(enemyBoard[r][c] == 'O' || enemyBoard[r][c] == '+')
					valid = false;

			}
		}

	}

	lastMove[0] = r;
	lastMove[1] = c;

	if(enemyBoard[r][c] == '@') {
		enemyBoard[r][c] = '+';
		utils.printBoard(enemyBoard);
		cout << "You've been hit by the enemy!\n\n";
		++enemyHits;
		lastMove[2] = 1;
	}

	if(enemyBoard[r][c] == '~') {
		enemyBoard[r][c] = 'O';
		utils.printBoard(enemyBoard);
		cout << "You were missed by the enemy.\n\n";
		lastMove[2] = 0;
	}

}
