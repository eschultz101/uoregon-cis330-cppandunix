// Eric Schultz
// 2/16/15
// CIS330 final project
// Battleship player header

class Player {
public:
	Player();
	void placeShips();
	void yourTurn(char**, char**);
	
	char** board;
	int enemyHits = 0;
};