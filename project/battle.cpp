// Eric Schultz
// 2/16/15
// CIS330 final project
// Battleship main driver

using namespace std;

#include "player.hpp"
#include "computer.hpp"
#include "utils.hpp"
#include <iostream>
#include <cstdio>

int main() {

	Utils utils;
	Player player;
	Computer comp;
	char c;

	cout << "Welcome to Battleship!\n\n";

	// allocates space for boards, initializes values
	player.board = utils.initBoard();
	comp.compBoard = utils.initBoard();
	comp.compShipBoard = utils.initBoard();
	cout << "Press anything to play...";
	getchar();

	// place ships method for both player and computer
	cout << "\nPlace your ships!\n\n";
	utils.printBoard(player.board);
	player.placeShips();
	comp.compPlaceShips();

	// printing both boards at start of game
	cout << "\n\tYour Ships\n";
	utils.printBoard(player.board);
	cout << "\tYour Targets\n";
	utils.printBoard(comp.compBoard);

	// actual player and computer turn loop
	while(player.enemyHits < 14 && comp.enemyHits < 14) {

		player.yourTurn(comp.compBoard, comp.compShipBoard);

		if(player.enemyHits == 14) {
			cout << "You win!" << endl;
			return 0;
		}

		cout << "Press anything to continue...\n";
		getchar(); getchar();

		comp.compTurn(player.board);

		if(comp.enemyHits == 14) {
			cout << "Computer wins!" << endl;
			return 0;
		}

	}

	return 1;

}