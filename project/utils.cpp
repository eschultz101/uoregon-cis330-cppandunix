// Eric Schultz
// 3/8/15
// CIS330 Final Project
// Input/Output Utilities

using namespace std;

#include "utils.hpp"
#include <iostream>
#include <iomanip>

Utils::Utils() {}

char** Utils::initBoard() {

	char** newBoard = new char* [10];
	for(int i = 0; i < 10; i++) {
		newBoard[i] = new char [10];
		for(int j = 0; j < 10; j++) {
			newBoard[i][j] = '~';
		}
	}

	return newBoard;

}

// Function Utils::checkPlacement
// Arguments: row head coordinate, row tail, column head, column tail, required length, board
// Returns: true if ships are placed in a valid manner, false if invalid
bool Utils::checkPlacement(int rh, int rt, int ch, int ct, int req, char** board) {

	if(rh > 9 || rh < 0 || rt > 9 || rt < 0 || ch > 9 || ch < 0 || ct > 9 || ct < 0)
		return false;

	if( rh != rt && ch != ct )
		return false;

	if(rh > rt) {
		if(rh - rt != req) return false;
		for(int i = rt; i <= rh; ++i) {
			if(board[i][ch] == '@') return false;
		}

	} else if(rt > rh) {
		if(rt - rh != req) return false;
		for(int i = rh; i <= rt; ++i) {
			if(board[i][ch] == '@') return false;
		}

	} else if(ch > ct) {
		if(ch - ct != req) return false;
		for(int i = ct; i <= ch; ++i) {
			if(board[rh][i] == '@') return false;
		}
	
	} else if(ct > ch) {
		if(ct - ch != req) return false;
		for(int i = ch; i <= ct; ++i) {
			if(board[rh][i] == '@') return false;
		}

	} else {	// the heck did they enter?!
		return false;	// its wrong!!!
	}

	return true;	// input passed all checks
}

void Utils::printBoard(char** board) {

	cout << "    1 2 3 4 5 6 7 8 9 10\n";
	for(int i = 0; i < 10; ++i) {
		cout << setw(3) << i + 1 << " ";
		for(int j = 0; j < 10; ++j) {
			cout << board[i][j] << " ";
		}
		cout << "\n";
	}
	cout << "\n\n";

}
